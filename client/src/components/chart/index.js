import React, {useEffect} from "react"
import {useState,useRef} from "react";
import * as echarts from 'echarts';
import moment from "moment";
import './view.css';

const Chart = () => {

    const lineChartRef = useRef();
    const barChartRef = useRef();
    const scatterChartRef = useRef();
    const [dataCount,setDataCount] = useState(0);
    const [xData, setXData] = useState([]);
    const [data,setData] = useState([]);

    useEffect(() => {
        getTime(5);// eslint-disable-next-line
    },[])

    useEffect(() => {
        if(dataCount>0) {
            getData();
        }
    },[dataCount])

    useEffect(() => {
        if(data.length) {
            initLineChart();
            initBarChart();
            initScatterChart();
            echarts.connect('timeGroup')
        }
    },[data])

    const getTime = (interval) => {
        const count = 24*60/interval;
        const result = []
        for(let i=0;i < count;i++) {
            const itemVal = moment().startOf('day').add(5 * i,'m').format('HH:mm');
            result.push(itemVal);
        }

        setDataCount(count);
        setXData(result);
    }

    const getData = () => {
        const res = [];
        for (let i = 0; i<dataCount;i++) {
            res.push(i*6);
        }
        setData(res);
    }

    const initLineChart = () => {
        const lineChart = echarts.init(lineChartRef.current);
        lineChart.group = 'timeGroup';
        const option = {
            xAxis: {
                type: 'category',
                data: xData
            },
            yAxis: {
                type: 'value'
            },
            tooltip: {
                show: true,
                trigger: 'axis'
            },
            series: [
                {
                    data,
                    type: 'line'
                }
            ]
        }
        lineChart.setOption(option);
    }

    const initBarChart = () => {
        const barChart = echarts.init(barChartRef.current);
        barChart.group = 'timeGroup';
        const option = {
            xAxis: {
                type: 'category',
                data: xData
            },
            yAxis: {
                type: 'value'
            },
            tooltip: {
                show: true,
                trigger: 'axis'
            },
            series: [
                {
                    data,
                    type: 'bar'
                }
            ]
        }
        barChart.setOption(option);
    }

    const formatScatterChart = () => {
      const arr = [];
      data.forEach((item,index) => {
          arr.push([xData[index],item]);
      })
      return arr
    }

    const initScatterChart = () => {
        const scatterData = formatScatterChart();
        const barChart = echarts.init(scatterChartRef.current);
        barChart.group = 'timeGroup';
        const option = {
            xAxis: {
                data:xData
            },
            yAxis: {},
            tooltip: {
                show: true,
                trigger: 'axis'
            },
            series: [
                {
                    data:scatterData,
                    symbolSize: 2,
                    type: 'scatter'
                }
            ]
        }
        barChart.setOption(option);
    }


    return (
        <div className={'chartBox'}>
            <div className={'lineChart'} ref={lineChartRef} />
            <div className={'barChart'} ref={barChartRef} />
            <div className={'scatterChart'} ref={scatterChartRef} />
        </div>
    )
}

export default Chart;
