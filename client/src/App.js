import Chart from "./components/chart";
import './App.css';

function App() {
  return (
    <div className="App">
      <Chart />
    </div>
  );
}

export default App;
